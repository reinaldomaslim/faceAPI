import os
import numpy as np
import urllib
import json
import cv2
import face_recognition
import pickle
import glob
import dlib
import base64
import logging
import io
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from urllib.request import urlopen
from face_recognition.face_recognition_cli import image_files_in_folder
from PIL import Image, ImageDraw
from sklearn import neighbors
from imutils import face_utils


FACE_DETECTOR_PATH = "{base_path}/cascades/haarcascade_frontalface_default.xml".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))

FACE_CLASSIFIER_PATH = "{base_path}/cascades/trained_knn_model.clf".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))

FACE_DATABASE_PATH = "{base_path}/faces/".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))

FACE_LANDMARK_PATH = "{base_path}/cascades/shape_predictor_68_face_landmarks.dat".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))

def train(train_dir=FACE_DATABASE_PATH, model_save_path=FACE_CLASSIFIER_PATH, n_neighbors=2, knn_algo='ball_tree', verbose=True):
    X, y = [], []

    for class_dir in os.listdir(train_dir):
        if not os.path.isdir(os.path.join(train_dir, class_dir)):
            continue

        for img_path in image_files_in_folder(os.path.join(train_dir, class_dir)):
            image = face_recognition.load_image_file(img_path)
            image = cv2.resize(image, (48, 48))
            encoded_face = face_recognition.face_encodings(image)
            if verbose and len(encoded_face) == 0:
                print(img_path)
                print('encoding failed')
                continue
            
            X.append(encoded_face[0])
            y.append(class_dir)

    if n_neighbors is None:
        n_neighbors = int(round(np.sqrt(len(X))))
        if verbose:
            print("Chose n_neighbors automatically:", n_neighbors)

    knn_clf = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors, algorithm=knn_algo, weights='distance')
    knn_clf.fit(X, y)

    if model_save_path is not None:
        with open(model_save_path, 'wb') as f:
            pickle.dump(knn_clf, f)

    return knn_clf

def predict(face_encoding, knn_clf=None, model_path=None, distance_threshold=0.6):
    if knn_clf is None and model_path is None:
        raise Exception("Must supply knn classifier either thourgh knn_clf or model_path")

    if knn_clf is None:
        with open(model_path, 'rb') as f:
            knn_clf = pickle.load(f)

    closest_distances = knn_clf.kneighbors(face_encoding, n_neighbors=1)
    if closest_distances[0] < distance_threshold:
        pred = knn_clf.predict(face_encoding) 

        return pred
    else:
        return "unknown"

def show_prediction_labels_on_image(img_path, predictions):
    pil_image = Image.open(img_path).convert("RGB")
    draw = ImageDraw.Draw(pil_image)

    for name, (top, right, bottom, left) in predictions:
        draw.rectangle(((left, top), (right, bottom)), outline=(0, 0, 255))
        name = name.encode("UTF-8")
        text_width, text_height = draw.textsize(name)
        draw.rectangle(((left, bottom - text_height - 10), (right, bottom)), fill=(0, 0, 255), outline=(0, 0, 255))
        draw.text((left + 6, bottom - text_height - 5), name, fill=(255, 255, 255, 255))
    del draw
    pil_image.show()

def classify(img):
    img = cv2.resize(img, (48, 48))
    try:
        encoded_face = face_recognition.face_encodings(img)[0]
    except:
        return "unknown"
    name = predict([encoded_face], model_path=FACE_CLASSIFIER_PATH)[0]
    return name

def _grab_image(path=None, stream=None, url=None, base64img=None):
    if path is not None:
        image = cv2.imread(path)
    else:   
        if url is not None:
            resp = urlopen(url)
            data = resp.read()
            image = np.asarray(bytearray(data), dtype="uint8")
        elif stream is not None:
            data = stream.read()
            image = np.asarray(bytearray(data), dtype="uint8")
        elif base64img is not None:
            content = base64img.split(",")
            data = base64.b64decode(content[1])
            image = np.fromstring(data, dtype="uint8")

        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    min_dim = min(image.shape[0], image.shape[1]) 
    if min_dim < 200:
        scaler = 200/min_dim
        image = cv2.resize(image, None, fx = scaler, fy = scaler)

    return image

@csrf_exempt
def detect(request):
    data = {"success": False}
    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        if body["image"] is not None:
            image = _grab_image(base64img=body["image"])
        else:
            if body["url"] is not None:
                image = _grab_image(url=body["url"])
            else:
                data["error"] = "No URL provided."
                return JsonResponse(data)

        detector = dlib.get_frontal_face_detector()
        rects = detector(image, 0)
        if len(rects) == 0:
            return JsonResponse(data)
        
        rects = [face_utils.rect_to_bb(rect) for rect in rects]
        rects = [(int(x), int(y), int(x + w), int(y + h)) for (x, y, w, h) in rects]
        names = [classify(image[rects[i][1]:rects[i][3], rects[i][0]:rects[i][2]]) for i in range(len(rects))]

        data.update({"num_faces": len(rects), "rects": rects, "names": names, "success": True})

    return JsonResponse(data)

@csrf_exempt
def collect(request):
    data = {"success": False}
    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        if body["image"] is not None:
            image = _grab_image(base64img=body["image"])
        else:
            if body["url"] is not None:
                image = _grab_image(url=body["url"])
            else:
                data["error"] = "No URL provided."
                return JsonResponse(data)

        name = body["name"]
        to_train = bool(int(body["train"]))

        detector = dlib.get_frontal_face_detector()
        predictor = dlib.shape_predictor(FACE_LANDMARK_PATH)
        fa = face_utils.FaceAligner(predictor, desiredFaceWidth=256)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        face_rects = detector(image, 0)

        rects = [face_utils.rect_to_bb(rect) for rect in face_rects]
        areas = [w*h for (x, y, w, h) in rects]
        rects = [(int(x), int(y), int(x + w), int(y + h)) for (x, y, w, h) in rects]
        i = np.argmax(areas)
        rect = rects[i]
        face_rect = face_rects[i]

        mask = np.zeros(image.shape[:2], dtype = np.uint8)
        hull = cv2.convexHull(face_utils.shape_to_np(predictor(image, face_rect)))
        cv2.fillPoly(mask, pts=[hull], color=1)
        img_masked = cv2.bitwise_and(image, image, mask = mask)
        face_img = cv2.resize(fa.align(img_masked, gray, face_rect), (256, 256))

        directory = FACE_DATABASE_PATH+name
        if not os.path.exists(directory):
            os.makedirs(directory)

        counter = len(glob.glob(directory+'*'))
        path = directory+'/'+str(counter)+'.jpg'
        cv2.imwrite(path, face_img)

        if to_train:
            train()

        data.update({"success": True})
    
    # return a JSON response
    return JsonResponse(data)



