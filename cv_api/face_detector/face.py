import cv2
import dlib
import numpy as np
import face_recognition
import time
import face_recognition_knn
import operator
from scipy import stats

from imutils import face_utils
from scipy.spatial import distance as dist
from filterpy.gh import GHFilter

# import vlc

FACE_CLASSIFIER_PATH = "{base_path}/cascades/trained_knn_model.clf".format(
    base_path=os.path.abspath(os.path.dirname(__file__)))

class Face(object):

    name = 'unknown'

    def __init__(self, face_rect, img):
        self.face_rect = face_rect
        self.names = {}

    def update(self, face_img, shape, euler_angle):
        self.img = face_img
        row, col = face_img.shape[:2]


    def classify_face(self, face_img):    
        face_img = cv2.resize(face_img, (48, 48))
    
        try:
            encoded_face = face_recognition.face_encodings(face_img)[0]
        except:
            return "unknown"
        
        name = face_recognition_knn.predict([encoded_face], model_path=FACE_CLASSIFIER_PATH)[0]

        if name != "unknown":
            if name in self.names:
                self.names[name] += 1
            else:
                self.names[name] = 1

            name = max(self.names.items(), key=operator.itemgetter(1))[0]


        return name
    