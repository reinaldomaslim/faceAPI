# FaceAPI

FaceAPI is a server API for face recognition. Supports two services:
- collect: input face photo with person's name
- detect: input photo, output face rectangle(s) [x_min, y_min, x_max, y_max] and name(s)

## Installation
Setup virtual environment (tested in python3.6)

```bash
virtualenv venv
pip install -r requirements.txt
```

## Usage

Open a terminal for the server
```bash
#start server
./run_server.sh
```

Run test set 
```bash
#start server
./test.sh
```
Check test.sh for queries

## Directories
- faceAPI/cv_api/face_detector/faces folder: stores each person's face photo
- faceAPI/cv_api/face_detector/cascades: stores trained weights and pre-trained embeddings

## License
[MIT](https://choosealicense.com/licenses/mit/)